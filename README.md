# Terraform AWS KeyPair

Generate an AWS KeyPair and output private key to a pem file. 

Key details not saved. For additional security location & name of the pk pem file could be stored in ENV vars

## Instructions
### initialise dependencies
terraform init

### check format
terraform fmt

### check execute
terraform plan

### execute 
terraform apply

### delete keys from state
terraform state rm tls_private_key.pk
terraform state rm aws_key_pair.kp

### delete tfstate backups if they contain pk / kp
del terraform.tfstate.*.backup
del terraform.tfstate.backup

For

